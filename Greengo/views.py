from sqlite3 import IntegrityError
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from Greengo.models import *
from Greengo.forms import *

# Create your views here.
def principal(request):
    return render(request, "home.html")

@csrf_exempt
def registrarse(request):
    if request.method == "GET":
        return render(request, "signup.html", {"form": UserCreationForm})
    else:
        if request.POST["password1"] == request.POST["password2"]:
            try:
                user = User.objects.create_user(
                    request.POST["username"], password=request.POST["password1"]
                )
                user.save()
                login(request, user)
                return redirect("tareas")
            except IntegrityError:
                return render(
                    request,
                    "signup.html",
                    {"form": UserCreationForm, "error": "Username existente."},
                )
        return render(
            request,
            "signup.html",
            {"form": UserCreationForm, "error": "Contraseñas ingresadas no coinciden."},
        )

@csrf_protect
@login_required
def tareas(request):
    print(login)
    print("ID de usuario:", request.user.id)
    return render(request, "tareas.html")

@csrf_exempt
def signin(request):
    if request.method == "GET":
        return render(request, "signin.html", {"form": AuthenticationForm})
    else:
        user = authenticate(
            request,
            username=request.POST["username"],
            password=request.POST["password"],
        )
        if user is None:
            return render(
                request,
                "signin.html",
                {
                    "form": AuthenticationForm,
                    "error": "Usuario o contraseña incorrectos.",
                },
            )

        login(request, user)
        return redirect("tareas")

@csrf_protect
@login_required
def signout(request):
    logout(request)
    return redirect("principal")

#Listamos usuarios además podemos editar y eliminar
@csrf_exempt
#@login_required
def listar_usuarios(request):
    usuarios = User.objects.all()
    if request.method == "POST":
        editarUsuario = request.POST.get("usuarioid")
        if request.POST.get("action") == "editar":
            return redirect("editar/" + str(editarUsuario))
        elif request.POST.get("action") == "eliminar":
            print("elimine al usuario", User.objects.filter(id=editarUsuario))
            User.objects.filter(id=editarUsuario).delete()
            usuarios = User.objects.all()
    return render(request, "listaUsuarios.html", {"usuarios": usuarios})

#Editamos usuario añadiendo más campos
@csrf_exempt
#@csrf_protect
#@login_required
def editarUsuario(request, id):
    print('id: ', id)
    editarUsuario = User.objects.get(id=id)
    print("usuario > ", editarUsuario)
    if request.method == "POST":
        if request.POST.get("action") == "editar":
            User.objects.filter(id=id).update(last_name=request.POST.get("modLastn"), first_name=request.POST.get("modName"), email=request.POST.get("modEmail"))
            print('guarde el usuario')
            return redirect("listar_usuarios")
    return render(request, "editarUsuario.html", {"usuarios": editarUsuario})


#Eventos
#@login_required
#def create_evento(request):
    if request.method == 'POST':
        form = eventoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("tareas") 
    else:
        form = eventoForm()
    return render(request, 'crearEvento.html', {'form': form})

#creamos eventos
@csrf_protect
@login_required
def create_evento(request):
    if request.method == 'GET':
        form = eventoForm()
        return render(request, 'crearEvento.html', {'form': form})
    else:
        form = eventoForm(request.POST)
        if form.is_valid():
            comentario = form.save(commit=False)
            comentario.usuario = request.user
            comentario.save()
            return redirect('tareas')
        else:
            print(form.errors)
    return render(request, 'crearEvento.html', {'form': form})

@csrf_exempt
#@csrf_protect
#@login_required
def listar_evento(request):
    eventos = Evento.objects.all()
    if request.method == "POST":
        editarEvento = request.POST.get("eventoid")
        if request.POST.get("action") == "editar":
            return redirect("editar/" + str(editarEvento))
        elif request.POST.get("action") == "eliminar":
            print("elimine el evento", Evento.objects.filter(codigo=editarEvento))
            Evento.objects.filter(codigo=editarEvento).delete()
            eventos = Evento.objects.all()
    return render(request, "listaEventos.html", {"eventos": eventos})

#editamos eventos
@csrf_protect
@login_required
def editarEvento(request, id):
    print('id: ', id)
    editarEvento = Evento.objects.get(codigo=id)
    print("evento > ", editarEvento)
    if request.method == "POST":
        if request.POST.get("action") == "editar":
            Evento.objects.filter(codigo=id).update(nombre=request.POST.get("modName"), descripcion=request.POST.get("modDesc"))
            print('guarde el evento')
            return redirect("tareas")
    return render(request, "editarEvento.html", {"evento": editarEvento})  